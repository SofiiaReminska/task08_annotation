package com.epam.lab.model;

import com.epam.lab.annotation.MyAnnotation;
import com.epam.lab.annotation.MyOwnAnnotation;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class Person {
    @MyOwnAnnotation()
    private int personId;
    @MyOwnAnnotation
    private String name;
    @MyOwnAnnotation
    private String surname;
    @MyAnnotation("20")
    private int age;
    @MyAnnotation("Lviv city")
    private String address;

    private String getFullName() {

        return String.format("%s %s", name, surname);
    }

    private String getFullName(String name, String surname) {

        return String.format("%s %s", name, surname);
    }

    private int countYearOfBirth(int age) {
        return LocalDate.now().getYear() - age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }

    private String myMethod(String a, int... args) {
        String result = "(String a, int... args)\n";
        String collectedInts = Arrays.stream(args).boxed().map(Objects::toString).collect(Collectors.joining());
        return result.concat(a).concat(collectedInts);
    }

    private String myMethod(String... args) {
        String result = "myMethod(String ... args)\n";
        for (String s : args) {
            result = result.concat(s);
        }
        return result;
    }
}
