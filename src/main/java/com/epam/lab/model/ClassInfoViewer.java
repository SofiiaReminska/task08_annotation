package com.epam.lab.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Arrays;

public class ClassInfoViewer {
    private static final Logger LOGGER = LogManager.getLogger(ClassInfoViewer.class);

    public static void main(String[] args) throws IllegalAccessException {
        ClassInfoViewer classInfoViewer = new ClassInfoViewer();
        Person person = new Person();
        classInfoViewer.printDetailsAboutClass(person);
    }

    private void printDetailsAboutClass(Object object) throws IllegalAccessException {
        Class clazz = object.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        LOGGER.info("Class info\n{}\n", classInfo(clazz));
        LOGGER.info("Fields info\n{}\n", fieldsInfo(clazz));
        LOGGER.info("Constructors info\n{}\n", constructorsInfo(clazz));
        LOGGER.info("Methods info\n{}\n", methodsInfo(clazz));
    }

    private String methodsInfo(Class clazz) {
        StringBuilder stringBuilder = new StringBuilder();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            int modifiers = method.getModifiers();
            if (method.getAnnotations() != null)
                stringBuilder.append(Arrays.toString(method.getAnnotations())).append("\n");
            stringBuilder.append(getModifiersString(modifiers));
            stringBuilder.append(method.getReturnType().getSimpleName()).append(" ");
            stringBuilder.append(method.getName()).append(" ");
            Parameter[] parameters = method.getParameters();
            stringBuilder.append(Arrays.toString(parameters)).append('\n');
        }
        return stringBuilder.toString();
    }

    private String getModifiersString(int modifiers) {
        StringBuilder stringBuilder = new StringBuilder();
        if (Modifier.isPrivate(modifiers))
            stringBuilder.append("private ");
        else if (Modifier.isProtected(modifiers))
            stringBuilder.append("protected ");
        else if (Modifier.isPublic(modifiers))
            stringBuilder.append("public ");
        if (Modifier.isStatic(modifiers))
            stringBuilder.append("static ");
        if (Modifier.isFinal(modifiers))
            stringBuilder.append("final ");
        return stringBuilder.toString();
    }

    private String constructorsInfo(Class clazz) {
        StringBuilder sb = new StringBuilder();
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            int modifiers = constructor.getModifiers();
            if (constructor.getAnnotations() != null)
                sb.append(Arrays.toString(constructor.getAnnotations())).append("\n");
            sb.append(getModifiersString(modifiers));
            sb.append(constructor.getName()).append(" ");
            Parameter[] parameters = constructor.getParameters();
            sb.append(Arrays.toString(parameters)).append('\n');
        }
        return sb.toString();
    }

    private String classInfo(Class clazz) {
        StringBuilder sb = new StringBuilder();
        Annotation[] annotations = clazz.getDeclaredAnnotations();
        if (annotations != null)
            sb.append(String.format("Class annotations: %s\n", Arrays.toString(annotations)));
        sb.append(getModifiersString(clazz.getModifiers()));
        sb.append("class ").append(clazz.getSimpleName()).append("\n");
        return sb.toString();
    }

    private String fieldsInfo(Object object) throws IllegalAccessException {
        Field[] fields = object.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (field.getAnnotations() != null)
                sb.append(Arrays.toString(field.getAnnotations())).append("\n");
            sb.append(getModifiersString(modifiers));
            sb.append(field.getType().getSimpleName()).append(" ");
            field.setAccessible(true);
            final Object obj = field.get(object);
            sb.append("value: ").append(obj).append("\n");
        }
        return sb.toString();
    }
}
