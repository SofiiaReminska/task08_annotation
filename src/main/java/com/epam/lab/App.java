package com.epam.lab;

import com.epam.lab.annotation.MyAnnotation;
import com.epam.lab.annotation.MyOwnAnnotation;
import com.epam.lab.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

/**
 * @author - Sofiia Reminska
 */
public class App {
    private static final Logger LOGGER = LogManager.getLogger(App.class);

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Class<Person> clazz = Person.class;
        printAnnotatedFieldsName(clazz);
        printAnnotationsValue(clazz);
        getFullNameWithReflection(clazz);
        setRandomFieldValue(clazz);
        invokeMyMethod(clazz);
    }

    private static void invokeMyMethod(Class<Person> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        final Person person = new Person();
        Method getMethodA = clazz.getDeclaredMethod("myMethod", String[].class);
        getMethodA.setAccessible(true);
        LOGGER.info(getMethodA.invoke(person, new Object[]{new String[]{"123", "321"}}));
        Method getMethodB = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
        getMethodB.setAccessible(true);
        LOGGER.info(getMethodB.invoke(person, "It is method B! ", new int[]{123, 321}));
    }

    private static void setRandomFieldValue(Class<Person> clazz) throws IllegalAccessException {
        final Field field = clazz.getDeclaredFields()[Math.abs(new Random().nextInt()) % clazz.getDeclaredFields().length];
        field.setAccessible(true);
        final Class<?> type = field.getType();
        final Person obj = new Person();
        if (type.equals(int.class)) {
            field.set(obj, 1);
        }
        if (type.equals(String.class)) {
            field.set(obj, "Hi!");
        }
        LOGGER.info(obj);
    }

    private static void printAnnotatedFieldsName(Class<Person> clazz) {
        Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(MyOwnAnnotation.class))
                .map(Field::getName)
                .forEach(LOGGER::info);
    }

    private static void printAnnotationsValue(Class<Person> clazz) {
        Arrays.stream(clazz.getDeclaredFields())
                .map(field -> field.getAnnotation(MyAnnotation.class))
                .filter(Objects::nonNull)
                .map(MyAnnotation::value)
                .forEach(LOGGER::info);
    }

    private static void getFullNameWithReflection(Class<Person> clazz) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final Person person = new Person();
        Field nameField = clazz.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(person, "Sofiia");
        Field surnameField = clazz.getDeclaredField("surname");
        surnameField.setAccessible(true);
        surnameField.set(person, "Reminska");
        Method getFullNameMethod = clazz.getDeclaredMethod("getFullName");
        getFullNameMethod.setAccessible(true);
        LOGGER.info((String) getFullNameMethod.invoke(person));
    }
}
